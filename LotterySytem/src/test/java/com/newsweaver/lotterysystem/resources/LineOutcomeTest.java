package com.newsweaver.lotterysystem.resources;

import com.newsweaver.lotterysystem.api.Line;
import org.junit.Test;


import java.util.List;

import static org.junit.Assert.*;

public class LineOutcomeTest {


    @Test
    public void When_GenerateLine_And_SumOfValuesIs2_Expect_OutcomeIs10() {

        Line line = new Line();

        List<Integer> values = line.getValues();

        int sum = 0;

        for (Integer value : values) {
            sum += value;
        }

        if (sum == (2)) {
            assertEquals(10, line.getOutcome());
        } else {
            assertNotEquals(10, line.getOutcome());
        }

    }

    @Test
    public void When_GenerateLine_And_AllValuesEqual_Expect_OutcomeIs5() {

        Line line = new Line();

        List<Integer> values = line.getValues();

        if (values.get(0).equals(values.get(1)) && values.get(1).equals(values.get(2))) {
            assertEquals(5, line.getOutcome());
        } else {
            assertNotEquals(5, line.getOutcome());
        }
    }

    @Test
    public void When_GenerateLine_And_SecondAndThirdValuesDifferentToFirstValue_Expect_OutcomeIs1() {

        Line line = new Line();

        List<Integer> values = line.getValues();

        if (!values.get(0).equals(values.get(1)) && !values.get(0).equals(values.get(2))) {
            assertEquals(1, line.getOutcome());
        } else {
            assertNotEquals(1, line.getOutcome());
        }

    }

}
