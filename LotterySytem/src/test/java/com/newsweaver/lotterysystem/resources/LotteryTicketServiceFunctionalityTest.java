package com.newsweaver.lotterysystem.resources;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class LotteryTicketServiceFunctionalityTest {

    @InjectMocks
    private LotteryTicketService lotteryTicketService;


    /**
     * tests for generateTicket
     **/

    @Test
    public void When_GenerateTicket_Expect_NotNull() {

        assertNotEquals(null, lotteryTicketService.generateTicket(1));
    }

    @Test
    public void When_GenerateTicket_And_FirstTime_Expect_SuccessMessage() {

        String successMessage = "Ticket generated with " + 1 + " lines";

        assertEquals(successMessage, lotteryTicketService.generateTicket(1));

    }

    @Test
    public void When_GenerateTicket_And_TicketExists_Expect_ErrorMessage() {

        String errorMessage = "Ticket already generated";

        lotteryTicketService.generateTicket(1);
        assertEquals(errorMessage, lotteryTicketService.generateTicket(1));
    }


    /**
     * tests for checkTicketStatus
     **/

    @Test
    public void When_CheckTicketStatus_And_TicketExists_Expect_NotNull() {

        lotteryTicketService.generateTicket(10);
        assertNotEquals(null, lotteryTicketService.checkTicketStatus());

    }

    @Test
    public void When_CheckTicketStatus_And_TicketDoesNotExist_Expect_Null() {

        assertEquals(null, lotteryTicketService.checkTicketStatus());
    }


    /**
     * tests for addTicketLines
     **/

    @Test
    public void When_AddTicketLines_Expect_NotNull() {

        assertNotEquals(null, lotteryTicketService.addTicketLines(1));
    }

    @Test
    public void When_AddTicketLines_And_TicketStatusNotChecked_Expect_SuccessMessage() {

        String successMessage = 1 + " new lines(s) added to ticket";

        lotteryTicketService.generateTicket(1);
        assertEquals(successMessage, lotteryTicketService.addTicketLines(1));

    }

    @Test
    public void When_AddTicketLines_And_TicketStatusChecked_Expect_ErrorMessage() {

        String errorMessage = "Cannot modify ticket after ticket status has been checked";

        lotteryTicketService.generateTicket(1);
        lotteryTicketService.checkTicketStatus();
        assertEquals(errorMessage, lotteryTicketService.addTicketLines(1));
    }


}