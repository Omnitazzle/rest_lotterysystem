package com.newsweaver.lotterysystem.resources;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

/**
 * TO DO
 **/
public class LotteryTicketServiceResourceTest {

    private HttpServer server;

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost/").port(8080).build();
    }

    @Before
    public void startServer() throws IOException {

        server = GrizzlyHttpServerFactory.createHttpServer(getBaseURI());
    }

    @After
    public void stopServer() {
        server.shutdownNow();
    }


    /**
     * TO DO
     **/
    @Test
    public void When_GenerateTicket_Expect_NotNull() {

        Client client = Client.create();

        WebResource webResource = client.resource("http://localhost:8080/lottery-ticket/generateTicket");

        ClientResponse response = webResource.accept("text/plain").post(ClientResponse.class);

    }

    /**
     * TO DO
     **/
    @Test
    public void When_GenerateTicket_And_FirstTime_Expect_SuccessMessage() {

        Client client = Client.create();

        WebResource webResource = client.resource("http://localhost:8080/lottery-ticket/generateTicket?numberOfLines=1");

        ClientResponse response = webResource.post(ClientResponse.class);

    }

}
